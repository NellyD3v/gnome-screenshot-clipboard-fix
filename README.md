# gnome-screenshot clipboard fix

In some situations gnome-screenshot does not save a screenshot of windows to the clipboard. This is a problem. This script is the solution, although perhaps not the best.

The script requires the gnome-screenshotter itself and xclip to work with the clipboard.

I think the best way to use it is to bind the `sh /path/to/make-a-screenshot.sh' command to the key so that it doesn't get too bothering.
