#!/bin/sh

gnome-screenshot -w -b -p -e shadow -f /tmp/latest_screenshot.png # make and save a screenshot of the window
xclip -selection clipboard -t image/png -i '/tmp/latest_screenshot.png' # copy into clipboard

# now we can paste this screenshot anywhere without problem :cool:
